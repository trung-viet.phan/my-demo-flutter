import 'dart:async';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class AddDeleteEdit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AddDeleteEditState();
}

class AddDeleteEditState extends State<AddDeleteEdit> {
  StreamController _streamController = StreamController<List<String>>.broadcast();

  final List<String> _list = new List<String>();
  String chosen = '';
  String chosen2 = '';
  int index = -1;
  var textController = new TextEditingController();

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  void _add() {
    setState(() {
      _list.add(chosen);
      index = index + 1;
    });
    textController.text = '';
  }

  void _delete() {
    _list.remove(chosen);
    _streamController.sink.add(_list);
  }

  void _edit() {
    _list.removeAt(index);
    _list.insert(index, chosen);
    print('haha $_list $index');
    _streamController.sink.add(_list);
  }

  void _clear() {
    _list.clear();
    _streamController.sink.add(_list);
  }

  Widget _buildRow(String s, int i) {
    final isCheck = chosen == s ? true : false;
    return new ListTile(
      title: Text(s, style: TextStyle(backgroundColor: isCheck ? Colors.blue : Colors.white),),
      onTap: () {
        setState(() {
          chosen = s;
          index = i;
        });
        textController.text = s;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            RaisedButton( onPressed: () => _add(), child: Text('Add'), color: Colors.blue,),
            RaisedButton( onPressed: () => _edit(), child: Text('Save'), color: Colors.red,),
            RaisedButton( onPressed: () => _delete(), child: Text('Delete'), color: Colors.green,),
            RaisedButton( onPressed: () => _clear(), child: Text('Clear'), color: Colors.yellow),
          ],),
        TextField(decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Enter text',

        ),
        controller: textController,
        onChanged: (s) {
          setState(() {
            chosen = s;
          });
        },),
        StreamBuilder(
          initialData: _list,
          stream: _streamController.stream,
          builder: (context, snapshot) {
            return ListView.builder(
                padding: EdgeInsets.all(10.0),
                itemCount: snapshot.data.length,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  print('i' +i.toString());
                  return _buildRow(snapshot.data.toList()[i], i);
                });
          },
        )
      ],
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      home: Scaffold(
        appBar: AppBar(
            title: Text('Haha :))))))))')
        ),
        body: Container(
          child: AddDeleteEdit(),
        ),
      ),
    );

  }
}
